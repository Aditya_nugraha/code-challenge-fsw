var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var session = require("express-session");
var flash = require("express-flash");


var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
var apiAuthRouter = require("./routes/api/auth");
var apiRoomRouter = require("./routes/api/room");

var app = express();
app.use(
  session({
    secret: "ini rahasia",
    resave: false,
    saveUninitialized: false,
  })
);

const passport = require("./lib/passport");
const passportJwt = require("./lib/passport-jwt");

app.use(flash());
app.use(passportJwt.initialize());
app.use(passport.initialize());
app.use(passport.session());

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/users", usersRouter);
app.use("/api/auth", apiAuthRouter);
app.use("/api/room", apiRoomRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
