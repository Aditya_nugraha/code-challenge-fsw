var express = require("express");
var router = express.Router();
const authController = require("../controllers/auth-controller");
const restrict = require("../middlewares/restrict");

/* GET home page. */
router.get("/", restrict, function (req, res, next) {
  res.render("index", { title: "Express" });
});
router.get("/register", (req, res) => res.render("register"));
router.post("/register", authController.registerAction);
router.get("/login", (req, res) => res.render("login"));
router.post("/login", authController.loginAction);
router.get("/session", authController.userSessionAction);

module.exports = router;
