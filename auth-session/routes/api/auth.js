var express = require("express");
var router = express.Router();
const authController = require("../../controllers/api/auth-controller");

const restrictJwt = require("../../middlewares/restrict-jwt");



// register page
router.post("/register", authController.registerAction);

// login page
router.post("/login", authController.loginAction);

router.get("/profile", restrictJwt, authController.profileAction);

module.exports = router;
