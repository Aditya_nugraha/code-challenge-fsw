var express = require("express");
var router = express.Router();
const authController = require("../../controllers/api/room-controller");

const restrictJwt = require("../../middlewares/restrict-jwt");





// create room
router.post("/create-room", authController.createRoomAction);

router.get("/profile", restrictJwt, authController.profileAction);

module.exports = router;
