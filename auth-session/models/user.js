"use strict";
const { Model } = require("sequelize");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

module.exports = (sequelize, DataTypes) => {
  class User extends Model {

    static associate(models) {
    }

    static #encrypt = function (password) {
      return bcrypt.hashSync(password, 10);
    };

    static register = function ({ username, password }) {
      const encryptedPassword = this.#encrypt(password);
      return this.create({ username, password: encryptedPassword });
    };

    checkPassword = (password) => bcrypt.compareSync(password, this.password);

    generateToken = () => {
      const payLoad = {
        id: this.id,
        username: this.username,
      };
      const secret = "rahasia";
      return jwt.sign(payLoad, secret);
    };

    static authenticate = async ({ username, password }) => {
      try {
        const user = await this.findOne({ where: { username } });
        if (!user) {
          return Promise.reject("Invalid Username & Password");
        }
        if (!user.checkPassword(password)) {
          return Promise.reject("Invalid Username & Password");
        }
        return Promise.resolve(user);
      } catch (error) {
        return Promise.reject(error);
      }
    };
  }

  User.init(
    {
      username: DataTypes.STRING,
      password: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "User",
    }
  );
  return User;
};
