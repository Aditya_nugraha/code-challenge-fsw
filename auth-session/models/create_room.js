'use strict';
const { Model } = require("sequelize");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

module.exports = (sequelize, DataTypes) => {
  class Create_Room extends Model {
    
    static associate(models) {
    }

    generateToken = () => {
      const payLoad = {
        id: this.id,
        room_name: this.room_name,
      };
      const secret = "rahasia";
      return jwt.sign(payLoad, secret);
    };

    static authenticate = async ({ room_name }) => {
      try {
        const Create_Room = await this.findOne({ where: { room_name } });
        if (!create_room) {
          return Promise.reject("Invalid Room Name");
        }
        return Promise.resolve(user);
      } catch (error) {
        return Promise.reject(error);
      }
    };
  }

  Create_Room.init({
    room_name: DataTypes.STRING,
    player1_id: DataTypes.STRING,
    player2_id: DataTypes.STRING,
    player1_hand: DataTypes.STRING,
    player2_hand: DataTypes.STRING,
    result: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Create_Room',
  });
  return Create_Room;
};