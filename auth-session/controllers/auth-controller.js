const { User } = require("../models");
const passport = require("passport");
const { render } = require("../app");

const registerAction = function (req, res, next) {
  User.register(req.body)
    .then(() => res.redirect("/login"))
    .catch((err) => next(err));
};

const loginAction = passport.authenticate("local", {
  successRedirect: "/",
  failureRedirect: "/login",
  failureFlash: true,
});

const userSessionAction = (req, res, next) => {
  res.render("session", { username: req.user.username });
};

module.exports = { registerAction, loginAction, userSessionAction };
