const { Create_Room } = require("../../models");

// const createRoomAction = (req, res, next) => {
//   Create_Room.register(req.body)
//     .then((create_room) => {
//       res.json({
//         id: create_room.id,
//         room_name: create_room.room_name,
//       });
//     })
//     .catch((err) => next(err));
// };

const createRoomAction = (req, res, next) => {
  Create_Room.authenticate(req.body)
    .then((create_room) => {
      res.json({
        id: create_room.id,
        room_name: create_room.room_name,
        token: create_room.generateToken(),
      });
    })
    .catch((err) => next(err));
};

const profileAction = (req, res, next) => {
  res.json(req.create_room);
};

module.exports = { createRoomAction, profileAction };
