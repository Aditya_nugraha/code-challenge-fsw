const { User } = require("../../models");

const registerAction = (req, res, next) => {
  User.register(req.body)
    .then((user) => {
      res.json({
        id: user.id,
        username: user.username,
      });
    })
    .catch((err) => next(err));
};

const loginAction = (req, res, next) => {
  User.authenticate(req.body)
    .then((user) => {
      res.json({
        id: user.id,
        username: user.username,
        token: user.generateToken(),
      });
    })
    .catch((err) => next(err));
};

const profileAction = (req, res, next) => {
  res.json(req.user);
};

module.exports = { registerAction, loginAction, profileAction };
