const express = require(`express`);
const app = express();
const port = 8000;

app.set(`view engine`, `ejs`);
app.use(express.static('Assets'));
app.use(express.urlencoded({ extended: false}));


app.get("/", (req, res) => res.render("home"));
app.get("/games", (req, res) => res.render("games"));

app.listen(port, () => console.log(`Web App Up in http://localhost:${port}`));