'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    queryInterface.addColumn('User_games', 'User_form_id', {
      type: Sequelize.INTEGER,
      references: {
        model: 'User_form',
        key: 'id',
      },
    });
  },

  down: async (queryInterface, Sequelize) => {
    queryInterface.removeColumn('User_games', 'User_form_id');
  },
};
