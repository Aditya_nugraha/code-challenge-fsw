const { json } = require("body-parser");
const express = require(`express`);
const app = express();
const port = 8000;

// let posts = require("./users.json");
// app.get("/users", (req, res) => {
//     res.status(200).json(posts);
//   });

app.use(express.json());

let users = require("./users.json");
app.get("/users", (req, res) => {
    res.status(200).json(users);
  });

app.post("/users", (req, res) => {
    let user = users.find(function (user) {
        return user.email === req.body.email &&
        user.password === req.body.password
    });
    if (!user){
        res.status(401).json("Invalid email. Please try again");
        return;
    }
    if (!user){
        res.status(401).json("Invalid password. Please try again");
        return;
    }
    res.status(200).json(user);
})


app.listen(port, () => console.log(`Web App Up in http://localhost:${port}`));