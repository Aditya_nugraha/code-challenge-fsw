'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class match_history extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  match_history.init({
    player_One: DataTypes.STRING,
    player_two: DataTypes.STRING,
    result: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'match_history',
  });
  return match_history;
};