'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User_form extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  User_form.init({
    full_name: DataTypes.STRING,
    gender: DataTypes.INTEGER,
    phone: DataTypes.INTEGER,
    nationality: DataTypes.STRING,
    team: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User_form',
  });
  return User_form;
};