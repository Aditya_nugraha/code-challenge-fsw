const express = require("express");
const app = express();
const port = 8000;
const { User_game } = require("../models");
const user_form = require("../models/user_form");

app.use(express.urlencoded({ extended: false }));
app.set("view engine", "ejs");
app.get("/User_games", (req, res) => {
    res.render('dashboard');
  });


// router user games dashboard (create)
app.post("/User_games", (req, res) => {
    User_game.create({
        user_name: req.body.user_name,
        email: req.body.email,
        password: req.body.password,
    }).then(function (User_game) {
        res.redirect("/User_games/user-list");
    });
});


// read
app.get("/User_games/user-list", (req, res) => {
    User_game.findAll().then((User_games) => {
      res.render("user/user-list", { User_games });
    });
});

app.get("/User_games/user-view/:id", (req, res) => {
    User_game.findOne({
      where: { id: req.params.id },
    }).then((User_game) => {
      if (!User_game) {
        res.status(404).send("Not found");
        return;
      }
  
      res.render("user/user-view", { User_game });
    });
  });

// update
  app.get("/User_games/user-update/:id", (req, res) => {
    User_game.findOne({
      where: { id: req.params.id },
    }).then((User_game) => {
      if (!User_game) {
        res.status(404).send("Not found");
        return;
      }

      res.render("user/user-update", { User_game });
    });
  });

  
  app.post("/User_games/user-update", (req, res) => {
    User_game.update({
        user_name: req.body.user_name,
        email: req.body.email,
        password: req.body.password,
      },
      {
        where: { id: req.body.id },
      }
    ).then(function (User_game) {
      res.redirect("/User_games/user-list");
    });
  });


// delete
  app.get("/User_games/delete/:id", (req, res) => {
    User_game.destroy({
      where: { id: req.params.id },
    }).then(function () {
      res.redirect("/User_games/user-list");
    });
  });
  


app.listen(port, () => console.log(`Web App Up in http://localhost:${port}`));
